Router.route('/', {
  name: 'colaboradores'
});

Router.route('/projetos', {
  controller: 'ProjetosController',
  action: 'projetos'
});


Router.route('/empresas', {
  controller: 'EmpresasAdminController',
  action: 'empresas'
});
if (Meteor.isClient) {
/*
  $('#myTab a[href="#profile"]').tab('show') // Select tab by name
  $('#myTab a:first').tab('show') // Select first tab
  $('#myTab a:last').tab('show') // Select last tab
  $('#myTab li:eq(2) a').tab('show') // Select third tab (0-indexed)
*/
  ApplicationController = RouteController.extend({
    layoutTemplate: 'AppLayout',

    onBeforeAction: function () {
      console.log('app before hook!');
      this.next();
    },

    action: function () {
      console.log('this should be overridden!');
    }
  });

  ColaboradoresController = ApplicationController.extend({
    action: function () {
      this.render('Colaboradores');
    }
  });

  ProjetosController = ApplicationController.extend({
    projetos: function () {
      this.render('Projetos');
    }
  });

  EmpresasAdminController = ApplicationController.extend({
    empresas: function () {
      this.render('Empresas');
    }
  });
}